<?php
    session_start();
    if(isset($_SESSION["email"])){
        $signedIn = true;
        $user = $_SESSION["email"];
        $lastLogin = $_SESSION["loginTime"];
        //header("location:login.php");
    }
    else {
        $signedIn = false;
        $user = null;
    }
?>

<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project Account Information Page
-->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'>
	<title>Mago</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<link href="stylesheets/style.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
    <!-- Navigation -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
             <!--    <a class="navbar-brand" href="about.php">About</a> -->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                        <input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                        <img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                        <img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <?php 
            if (isset($_GET['change'])) {
                displaychange();
            } 
            elseif ($signedIn==true){
                showLoggedIn();
            }
            else{ //(isset($_GET['login'])){
                displaylogin();
            }
            // else if(isset($_GET[''])){
                
            // }        
        ?>
        <?php

            function displaychange(){
                echo '
                <fieldset>
                    <legend>Update your password below!</legend>
                    <form method="POST" action="include/dboperation.php">                   
                        <input class="info" type="text" placeholder="BC Email" name="email" id="emailEntry">
                        <br><br>
                        <input class="info" type="password" placeholder="Current Password" name="oldPassword" id="oldPasswordEntry">
                        <br><br>
                        <input class="info" type="password" placeholder="New Password" name="newPassword" id="passwordChange1">
                        <br><br>
                        <input class="info" type="password" placeholder="Confirm New Password" name="newPassword2" id="passwordChange2">
                        <br><br>
                        <input class="btn btn-primary btn-lg btn-join" type="submit" name="updatePW" value="Update Password">
                    </form>
                </fieldset>
                        ';
                }


            function showLoggedIn(){

                $user = $_SESSION["email"];
                $lastLogin = $_SESSION["loginTime"];

                echo "
                    <h1>Account Settings</h1>
                    <hr>

                    <h3>$user has been logged in!</h3>
                    <h3>Last login was $lastLogin</h3>
                    <br><br><br>
                ";


                displaychange();
            }

            function displaylogin(){
                echo '
                <fieldset>
                    <legend>Enter your username and password to log on!</legend>
                    <form method="POST" action="include/dboperation.php">                   
                        <input class="info" type="text" placeholder="BC Email" name="email" id="adminEmail">
                        <br><br>
                        <input class="info" type="password" placeholder="Password" name="password" id="adminPW">
                        <br><br>
                        <input class="btn btn-primary btn-lg btn-join" type="submit" name="login" value="Login">
                        <input class="btn btn-primary btn-lg btn-join" type="submit" name="forgotPW" value="Forgot Password">
                    </form>
                </fieldset>
                        ';

            }
        ?>
        <hr>
            <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Mago 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
</body>
</html>