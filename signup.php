<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project SignUp Page
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'>
	<title>Mago</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<link href="stylesheets/style.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
    <script src="javascripts/app.js" type="text/javascript"></script>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-12">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>

                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                        <input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                        <img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                    	<img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <?php 
            // if (isset($_GET['join'])) {
                displayform();      
        ?>
        <?php
        function displayform(){
            echo '
                <fieldset>
                    <legend>Enter your information below to become a member!</legend>
                    <form method="POST" onsubmit="return validateSignUp()" action="include/dboperation.php" >
                        <input class="info" type="text" placeholder="Name" name="name" id="nameEntry">
                        <article id="nameError"></article>
                        <br><br>
                        <input class="info" type="text" placeholder="BC Email" name="email" id="emailEntry">
                        <article id="emailError"></article>
                        <br><br>
                        <input class="info" type="password" placeholder="Password" name="password" id="passwordEntry1">
                        <article id="pwError"></article>
                        <br><br>
                        <input class="info" type="password" placeholder="Confirm Password" name="password2" id="passwordEntry2">
                        <br><br>
                        <input class="btn btn-primary btn-lg" type="submit" name="signup" value="Submit!">
                    </form>
                </fieldset>';

            }
        ?>
        <hr>
            <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Mago 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
</body>
</html>