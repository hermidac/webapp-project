function validateSignUp(){
	var name = document.getElementById("nameEntry").value;
	var email = document.getElementById("emailEntry").value;
	var pw1 = document.getElementById("passwordEntry1").value;
	var pw2Id = document.getElementById("passwordEntry2").value;

	//console.log(name);
	//console.log(email);
	//console.log(pw1);
	nameEr = validateName(name);
	emailEr = validateEmail(email);
	pwEr = validatePW(pw1);
	if( (nameEr==true) && (emailEr==true) && (pwEr==true) ){
		return true;
	}
	else {
		return false;
	}
}

function validateMail(){
	var subject = document.getElementById("emailSubject").value;
	var message = document.getElementById("emailMessage").value;
	var mailpassword = document.getElementById("emailPassword").value;

	subjectEr = validateLength(subject, "subjectError");
	messageEr = validateLength(subject, "messageError");
	mailPasswordEr = validateLength(subject, "mailPasswordError");

	if( (subjectEr==true) && (messageEr==true) && (mailPasswordEr==true) ){
		return true;
	}
	else {
		return false;
	}
}

function validateCheckboxes(){
	var checkbox1 = document.getElementById('checkbox1').checked==true;
	var checkbox2 = document.getElementById('checkbox1').checked==true;
	var checkbox3 = document.getElementById('checkbox1').checked==true;

	if( (checkbox1) || (checkbox2) || (checkbox3) ){
		return true;
	}
	else{
		writetoelement("checkBoxError", "Error select a checkbox", "red");
		return false;
	}
}

function validateLength(str, errorID){
	if(str.length<1){
		writetoelement(errorID, "Error field is empty", "red");
		return false;
	}
	return true;
}

function validateName(name){
	var tomatch = /^[a-z ,.'-]+$/i;
	if ( (!tomatch.test(name)) || (name.length<5)) {

		writetoelement("nameError", "Please enter a valid first and last name", "red");
		console.log("Name is wrong");
		return false;
	}
	writetoelement("nameError","","red");
	return true; 
}

function validateEmail(email){
	if( (email.substr(-7)!="@bc.edu") || (email.length<9)){
		writetoelement("emailError",
					"Please enter a valid BC email",
					"red");
		console.log("Email is wrong");
		return false;
	}
	writetoelement("emailError","","red");
	return true; 
}

function validatePW(pw){
	if(pw.length<4){
		writetoelement("pwError",
				"Please enter a valid Password",
				"red");
		console.log("PW is wrong");
		return false;
	}
	return true;
}

function writetoelement(elementname, message, color){
		var resultloc=document.getElementById(elementname);
		resultloc.style.color = color;
		resultloc.innerHTML =  message;
    }
