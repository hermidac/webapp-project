<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'>
	<title>About Mago</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

	<link href="stylesheets/style.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                    	<input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                    	<img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                    	<img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<h1>
Discover Mago
</h1>
<br><br>
<p>
Mago is an online music marketplace that allows content creators to sell media online without seller's fees.
With Mago, artists don't have to go through aggregating services and are not forced to accept the given price rates.
This peer-to-peer marketplace uses digital currency to help content creators keep their revenue, set their own prices,
and not have to worry about fraud.
</p>
<br>
<br>
<p>
Artists: upload your content, link a digital currency wallet, and track your sales.
Music lovers: purchase your favorite artists' music with the guarantee that your money is supporting their art, 100% of the time.
</p>

</body>

</html>