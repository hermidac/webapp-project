<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project HomePage
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'>
	<title>Mago</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<link href="stylesheets/style.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                    	<input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                    	<img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                    	<img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <header class="jumbotron hero-spacer">
            <h1>Welcome to Mago!</h1>
            <p>Your marketplace for music.</p>
            <p><a href="search.php" class="btn btn-primary btn-large">Explore</a>
            </p>
        </header>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Featured Artists</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img class="sampleImgs" src="images/foofighters.jpg" alt="Foo Fighters">
                    <div class="caption">
                        <h3>Foo Fighters</h3>
                        <p>Sonic Highways</p>
                        <p>
                            <a href="search.php?searchtxt=Foo+Fighters&op.x=0&op.y=0&op=Search" class="btn btn-primary">View</a> <!-- <a href="#" class="btn btn-default">More Info</a> -->
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img class="sampleImgs" src="images/nodoubt.jpg" alt="No Doubt">
                    <div class="caption">
                        <h3>No Doubt</h3>
                        <p>Tragic Kingdom</p>
                        <p>
                            <a href="search.php?searchtxt=No+Doubt&op.x=0&op.y=0&op=Search" class="btn btn-primary">View</a> <!-- <a href="#" class="btn btn-default">More Info</a> -->
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img class="sampleImgs" src="images/nineinchnails.jpg" alt="Nine Inch Nails">
                    <div class="caption">
                        <h3>Nine Inch Nails</h3>
                        <p>Year Zero</p>
                        <p>
                            <a href="search.php?searchtxt=Nine+Inch+Nails&op.x=0&op.y=0&op=Search" class="btn btn-primary">View</a> <!-- <a href="#" class="btn btn-default">More Info</a> -->
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img class="sampleImgs" src="images/thewho.jpg" alt="The Who">
                    <div class="caption">
                        <h3>The Who</h3>
                        <p>My Generation</p>
                        <p>
                            <a href="search.php?searchtxt=The+Who&op.x=0&op.y=0&op=Search" class="btn btn-primary">View</a> <!-- <a href="#" class="btn btn-default">More Info</a> -->
                        </p>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <!-- /.row -->
        <hr>
        <br>
    </div>

        <!-- Footer -->
        <div class="footer">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Mago 2015</p>
                </div>
            </div>
        </div>
    <!-- /.container -->
	</body>
</html>