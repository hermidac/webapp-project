<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project Search Page
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <title>Mago</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <link href="stylesheets/style.css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>

                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                        <input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                        <img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                        <img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <?php 
            if (isset($_GET['searchtxt']) && strlen($_GET['searchtxt'])>1) {
                displaySearchResults();
            } 
            // else if (isset($_GET['advancedSearch'])){ 
            // }

            else {
                displaySearch();
            }  
        ?>
        <?php


            function displaySearchResults(){
                error_reporting(E_ALL ^ E_NOTICE);
                date_default_timezone_set('EST');
                $searchString = (String) $_GET['searchtxt'];
                $searchExp = $_GET['searchtxt'];
                $searchExp = str_replace(" ", "+", $searchExp);
                //echo $searchExp;
                $itunesURL = "https://itunes.apple.com/search?term=".$searchExp."&entity=musicTrack&limit=200";
                //echo $itunesURL;
                $json = file_get_contents($itunesURL);
                $data = json_decode($json, true);
                //var_dump($data);
                //echo $data;

                echo "<h1>Results for $searchString </h1>";
                echo'
                    <table class="table table-striped table-hover">
                        <tr>
                            <th class=\'song\'>Song</th>
                            <th>Artist</th>
                            <th></th>
                            <th class=\'album\'>Album</th>               
                            <th>Release</th>
                            <th>Preview</th>
                            <th>Purchase Link</th> 
                        </tr>
                        ';
                $count = $data['resultCount'];
                //echo "$count";
                for($i = 0; $i<$count; $i++){
                    $artistImg = $data['results'][$i]['artworkUrl100'];
                    $artistId = $data['results'][$i]['artistId'];
                    $trackName = $data['results'][$i]['trackName'];
                    $artistName = $data['results'][$i]['artistName'];
                    $collectionName = $data['results'][$i]['collectionName'];

                    $DateStringIso8601 = $data['results'][$i]['releaseDate'];
                    $DateIso8601 = strtotime($DateStringIso8601);
                    $release = date('M Y', $DateIso8601);
                    $preview = $data['results'][$i]['previewUrl'];
                    $purchase = $data['results'][$i]['trackViewUrl'];

                    echo "<tr>
                        <td class='song'>$trackName</td>
                        <td class='artist'><a href=\"artist.php?artisttxt=$searchExp&artistId=$artistId\" class=\"btn btn-primary\">$artistName</a></td>
                        <td class='albumCover'><img src=$artistImg alt='artist image'></td>
                        <td class='album'>$collectionName</td>
                        <td class='release'>$release</td>
                        <td class='previewLink'><a href=$preview class=\"btn btn-primary\">Preview</a></td>
                        <td class='purchaseLink'><a href=$purchase class=\"btn btn-primary\">Purchase</a></td>
                    </tr>";
                }

            }

            function displaySearch(){
                echo'
                    <div class="searchBox">
                    <h1>Search for an Artist</h1>
                    <hr>
                    <fieldset>
                        <form method="GET">
                        <input class="info searchField" type="text" placeholder="Enter Artist" name="searchtxt" id="searchEntry">
                        <br><br>
                        <input class="btn btn-primary btn-lg btn-join searchBtn" type="submit" name="op" value="Search">
                        </form>
                    </fieldset>
                    </div>
                ';
            }

            // function displayAdvancedSearch(){
            //     echo '
            //     <fieldset>
            //         <legend>Search for your favorite artist below!</legend>
            //         <form method="GET">                   
            //             <input class="info" type="text" placeholder="Artist" name="artistName" id="artistEntry">
            //             <br><br>
            //             <input class="info" type="text" placeholder="Song" name="songName" id="songEntry">
            //             <br><br>
            //             <label>Include Explicit Songs</label>
            //             <select>                            
            //                 <option value="Yes">Yes</option>
            //                 <option value="No">No</option>
            //             </select>
            //             <br><br>
            //             <input class="btn btn-primary btn-lg btn-join" type="submit" name="advancedSearch" value="Advanced Search">
            //         </form>
            //     </fieldset>
            //             ';
            // }
        ?>

    <br>
    <br>
    </div>

        <!-- Footer -->
        <div class="footer">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Mago 2015</p>
                </div>
            </div>
        </div>
    <!-- /.container -->
</body>
</html>