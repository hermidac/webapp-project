<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project Artist Page
-->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset='utf-8'>
  <title>Mago</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

  <link href="stylesheets/style.css" rel="stylesheet"/>
  <link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="account.php">Login</a>
                <a class="navbar-brand" href="signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="about.php">About</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                      <input class="submit" type="image" name="op" value="Search" img src="images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                      <img src="images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                      <img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <?php
      $artistId = (String) $_GET['artistId'];
      $artistName = $_GET['artisttxt'];
      $searchExp = $_GET['artisttxt'];
      $searchExp = str_replace(" ", "+", $searchExp);
      $echoNestTwitterURL = "http://developer.echonest.com/api/v4/artist/twitter?api_key=HL1R2EDBRUPTJDIF6&format=json&name=".$searchExp;
      $echoNestImageURL = "http://developer.echonest.com/api/v4/artist/images?api_key=HL1R2EDBRUPTJDIF6&format=json&results=1&start=0&license=unknown&name=".$searchExp;
      // $itunesURL = "https://itunes.apple.com/lookup?id=".$artistId."&entity=album&limit=5";
      $itunesURL = "https://itunes.apple.com/lookup?id=".$artistId."&entity=album&limit=5";

      $itunesResponse = json_decode(file_get_contents($itunesURL), true);
      $enTwitterResponse = json_decode(file_get_contents($echoNestTwitterURL), true);
      $enImageResponse = json_decode(file_get_contents($echoNestImageURL), true);

      $itunesData = $itunesResponse['results'];

      $enTwitterResponse = $enTwitterResponse['response']['artist'];
      if(array_key_exists('twitter', $enTwitterResponse)){
        $twitterHandle = $enTwitterResponse['twitter'];
      }
      else {
        $twitterHandle = null;
      }        

      $enImageResponse = $enImageResponse['response']['images'][0]['url'];


      function createAlbumRow($itunesJSON){
        echo '<div class="row text-center">';
        // var_dump($itunesJSON);
        for ($i=1;$i<5;$i++){
          createAlbumColumn($itunesJSON[$i]);
        }
        echo '</div>
        <!-- /.row -->
        ';
      }


      function createAlbumColumn($albumJSONdecoded){
        $imageURL = $albumJSONdecoded['artworkUrl100'];
        $albumName = $albumJSONdecoded['collectionName'];
        $purchaseURL = $albumJSONdecoded['collectionViewUrl'];
        $artistName = $albumJSONdecoded['artistName'];

        echo "
            <div class=\"col-md-3 col-sm-6 hero-feature\">
              <div class=\"thumbnail albums\">
                <img src=$imageURL alt=\" \">
                    <div class=\"caption\">
                      <h3>$albumName</h3>
                      <p>$artistName</p>
                      <p><a href=$purchaseURL class=\"btn btn-primary purchase\">Purchase</a><p>
                    </div>
                  </div>
                </div>
        ";
      }


        function displayArtistResults(){
            date_default_timezone_set('EST');
            $searchString = (String) $_GET['artisttxt'];
            $searchExp = $_GET['artisttxt'];
            $searchExp = str_replace(" ", "+", $searchExp);
            //echo $searchExp;
            $itunesLink = "https://itunes.apple.com/search?term=".$searchExp."&entity=musicTrack&limit=200";
            //echo $itunesURL;
            $json = file_get_contents($itunesLink);
            $data = json_decode($json, true);
            //var_dump($data);
            //echo $data;

            echo "<h1>Results for $searchString </h1>";
            echo'
                <table class="table table-striped table-hover">
                    <tr>
                        <th class=\'song\'>Song</th>
                        <th>Artist</th>
                        <th></th>
                        <th class=\'album\'>Album</th>               
                        <th>Release</th>
                        <th>Preview</th>
                        <th>Purchase Link</th> 
                    </tr>
                    ';
            $count = $data['resultCount'];
            //echo "$count";
            for($i = 0; $i<$count; $i++){
                $artistImg = $data['results'][$i]['artworkUrl100'];
                $artistId = $data['results'][$i]['artistId'];
                $trackName = $data['results'][$i]['trackName'];
                $artistName = $data['results'][$i]['artistName'];
                $collectionName = $data['results'][$i]['collectionName'];

                $DateStringIso8601 = $data['results'][$i]['releaseDate'];
                $DateIso8601 = strtotime($DateStringIso8601);
                $release = date('M Y', $DateIso8601);
                $preview = $data['results'][$i]['previewUrl'];
                $purchase = $data['results'][$i]['trackViewUrl'];

                echo "<tr>
                    <td class='song'>$trackName</td>
                    <td class='artist'><a href=\"artist.php?artisttxt=$searchExp&artistId=$artistId\" class=\"btn btn-primary\">$artistName</a></td>
                    <td class='albumCover'><img src=$artistImg alt='artist image'></td>
                    <td class='album'>$collectionName</td>
                    <td class='release'>$release</td>
                    <td class='previewLink'><a href=$preview class=\"btn btn-primary\">Preview</a></td>
                    <td class='purchaseLink'><a href=$purchase class=\"btn btn-primary\">Purchase</a></td>
                </tr>";
            }

        }


    ?>

    <!-- Page Content -->
    <div class="container">
        <hr>

        <!-- Title
        <div class="row">
            <div class="col-lg-12">
                <h3>Latest Features</h3>
            </div>
        </div>

        Page Features -->
      	<div class="container">
          <div class="jumbotron" align="center">
            <h1><?php echo $artistName ?></h1> 
            <div class="container">
              <div class="left">
                <img width="400px" height="400px" src="<?php echo $enImageResponse?>" alt="">
              </div>
              <div class="right">

                <?php echo "
                  <a class=\"twitter-timeline\"  data-widget-id=\"598411591948828672\" width=\"400px\" height=\"400px\" href=\"https://twitter.com/$twitterHandle\" data-screen-name=\"$twitterHandle\">Tweets by @$twitterHandle</a>
                ";

                ?>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
              </div>
          </div>
        </div>
      </div>

      <?php
        error_reporting(E_ALL ^ E_NOTICE);
        createAlbumRow($itunesData);
        echo '<br><br>';
        displayArtistResults();
       ?>


        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Mago</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

</body>
</html>
