<?php
    session_start();
    include('dbconn.php');
    if(isset($_SESSION['email'])){
        $signedIn = true;
        $user = $_SESSION['email'];
        $lastLogin = $_SESSION['loginTime'];
        //header("location:login.php");
    }
    else {
        $signedIn = false;
        $user = null;
    }
    //ini_set("display_errors", "off");
?>

<!--
Katie Coleman, Chris Hermida, Luiza Justus 
Web App Dev TTH 1:30
Final Project DB file
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'>
	<title>Mago</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="../js/app.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<link href="../stylesheets/style.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Play' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Candal|Arbutus|Play' rel='stylesheet' type='text/css'>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">Home</a>
                <a class="navbar-brand" href="../account.php">Login</a>
                <a class="navbar-brand" href="../signup.php">Sign Up</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../about.php">About</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav search">
                    <li>
                        <form method="get" action="../search.php">
                        <!-- <input class="btn btn-primary btn-lg btn-join" type="submit" name="op" value="Add Attraction"> -->
                        <input class="info" type="text" placeholder="Enter an Artist or Song to search for!" name="searchtxt" id="searchEntry">
                    </li>   
                    <li>
                    	<input class="submit" type="image" name="op" value="Search" img src="../images/search.jpg" alt="Search" width="50px" height="50px">
                    </li>
                        </form>
                    <li>
                    	<img src="../images/shoppingcart.jpg" alt="Cart" width="50px" height="50px">
                        <!-- <a href="#">Cart</a> -->
                    </li>
                
<!--                      <li>
                    	<img src="images/messages.jpg" alt="Messages" width="50px" height="50px">
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>  

<?php

	if (isset($_POST['forgotPW'])){
		handleForgotPW();
			}

    elseif (isset($_POST['signup'])){
		handleSignUp();		
	}

	elseif (isset($_POST['login'])){
		handleLogin();
	}

	function handleSignUp(){
		$dbc = connectToDB('hermidac');
		$name = $_POST['name'];
		$email = $_POST['email'];
		//password confirmation is checked through Javascript form validation
		$pwhash1 = sha1($_POST['password']);
		$pwhash2 = sha1($_POST['password2']);
		if(getEmailCount($email)==0){
			$InsertQuery = "INSERT INTO Mago (Name, Email, Password, Last_Login, Signup_Date)
			VALUES ('$name', '$email', '$pwhash1', NOW(), NOW());";
			$result = performQuery($dbc, $InsertQuery);
			disconnectFromDB($dbc, $result);
			echo'
				<h1>Thanks for signing up!</h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" name="back" value="Go Back">
			    </form>';
		}
		else {
			echo'<h1>Error, email already exists</h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" name="back" value="Go Back">
			    </form>';
			}
		}

	function handleForgotPW(){
		$dbc = connectToDB('hermidac');
		if(isset($_POST['email'])){
			$email = $_POST['email'];
			if(getEmailCount($email)==1){
				newPassword($dbc, $email);
			}
			else {
				echo'<h1>Error, email doesn\'t exist</h1>
					<form action="../index.php">
						<input class="btn btn-primary btn-lg" type="submit" name="back" value="Go Back">
				    </form>';
			}
		}
		else {
			echo'<h1>Error, email doesn\'t exist</h1>
				<form action="../index.php">
						<input class="btn btn-primary btn-lg" type="submit" name="back" value="Go Back">
				</form>';

		}
		disconnectFromDB($dbc, $result);
	}

	function newPassword($dbc, $email){
		$randnum = rand(1111111111,9999999999);
		$to=$email;
		$subject="Mago Password Reset";
		$body="Your new temporary password is $randnum";
		$headers = 'From: hermidac@bc.edu';

		if ( mail($to, $subject, $body, $headers) ){
			//echo " Mail was sent $to, $subject, $body, $headers ";
			$pwhash = sha1($randnum);
			$result = changePassword($dbc, $email, $pwhash);
			echo'<h1>Success! Your lost password has been reset, check your inbox!</h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" value="Back">
	    		</form>';
		}
		else{
			echo '<h1> Error Email was NOT sent but password was changed </h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" value="Back">
	    		</form>';
		}
		//disconnectFromDB($dbc, $result);
	}

	function getEmailCount($email){
		$dbc = connectToDB('hermidac');
		$EmailCheckQuery = "SELECT COUNT(*) as count FROM Mago WHERE Email='$email' ";
		$result = performQuery($dbc, $EmailCheckQuery);

		//error checking for duplicate emails
		$EmailCount = mysqli_fetch_assoc($result);
		// $EmailCount = (int) $EmailCount;
		$EmailCount = $EmailCount['count'];
		//echo $EmailCount;
		return $EmailCount;
		// if( (int)$EmailCount!=1){
		// 	die('<h1>Error the email address you entered does not exist!</h1>
		// 	<form action="../index.php">
		// 		<input class="btn btn-primary btn-lg" type="submit" name="change" value="Try Again">
		//     </form>');
		// 	}
	}

	function handleLogin(){
		$dbc = connectToDB('hermidac');
		if( (isset($_POST['email'])) and (getEmailCount($_POST['email'])==1) ){
			$email = $_POST['email'];
			$pwhash = sha1($_POST['password']);
			//echo "Attempting to check password";
			$login = checkPassword($dbc, $email, $pwhash);
			//echo "The password is correct: $login";
			if($login==true){
				//echo "Attempting to update Last Login Time";
				$result = changePassword($dbc, $email, $pwhash);
				//echo "Successfully logged on";

				date_default_timezone_set('America/New_York');
   				$currenttime = date('h:i:s:u');
				$_SESSION["loginTime"] = $currenttime;
				$_SESSION["email"]= $email;
				$_SESSION["login"]= $login;
				header("location:../account.php");
				disconnectFromDB($dbc, $result);
			}
			else {
				echo '<h1> Error invalid password or email </h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" value="Back">
	    		</form>';
			}
		}
		else {
			echo '<h1> Error invalid password or email </h1>
				<form action="../index.php">
					<input class="btn btn-primary btn-lg" type="submit" value="Back">
	    		</form>';
			}

		}

	function changePassword($dbc, $email, $newPasswordHash){
			$updatePasswordQuery = "UPDATE Mago
				SET Password='$newPasswordHash', Last_Login=NOW()
				WHERE Email='$email'";
			$result = performQuery($dbc, $updatePasswordQuery);	
			return $result;		
	}

	function checkPassword($dbc, $email, $passwordHash){
		$passwordQuery = "SELECT Password FROM Mago WHERE Email='$email'";
		$result = performQuery($dbc, $passwordQuery);
		while ($row = mysqli_fetch_assoc($result)) {
	     	//print_r ($row);
	     	if($row['Password']==$passwordHash){
	     		return TRUE;		
	     	}
		}
		return FALSE;
	}

	function buildTable($dbc){
		echo'
		<table class="table table-striped table-hover">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Password</th>				
				<th>Last_Login</th>
				<th>Signup_Date</th>
			</tr>
		';

		$tableQuery = "SELECT * FROM Mago";
		$result = performQuery($dbc, $tableQuery);
		while($row = mysqli_fetch_assoc($result)){
    		echo "<tr>";
    		foreach($row as $item)
        		echo "<td>$item</td>";
    		echo "</tr>";
		}
		echo '</table><br><br>';
	}

	// function sendMail($dbc){
	// 	$tableQuery = "SELECT * FROM Mago";
	// 	$result = performQuery($dbc, $tableQuery);
	// 	$subject = $_POST['mailSubject'];
	// 	$message = $_POST['mailMessage'];
	// 	$mailpw= $_POST['mailPassword'];
	// 	$headers = 'From: hermidac@bc.edu';

		
	// 	while($row = mysqli_fetch_assoc($result)){
	// 		$to = $row['email'];
	// 		$genre = $row['genre'];
	// 		if(in_array($membership, $type)){
	// 			mail($to, $subject, $body, $headers);	
	// 			// echo "To is: $to";
	// 			// echo "Membership type is: $membership";	
	// 		}		
	// 	}

	// 	disconnectFromDB($dbc, $result);
	// 		die('<h1>Success! Sent out the group message!</h1>
	// 			<form action="../index.php">
	// 				<input class="btn btn-primary btn-lg" type="submit" value="Back">
	//     		</form>');
	// }

?>

</body>
</html>